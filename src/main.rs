#[macro_use]
extern crate diesel;

use warp::Filter;
use dotenv::dotenv;
use std::io::{stdin, Read};

mod db;
mod models;
mod schema;

use models::user::User;

#[tokio::main]
async fn main() {
    dotenv().ok();

    let hello = warp::path!("hello")
        .map(|| {
            let db_connection = db::connection::establish_connection();
            let users = User::get_all(&db_connection);

            warp::reply::json(&users)
        });

    warp::serve(hello)
        .run(([127, 0, 0, 1], 3000))
        .await;
}
