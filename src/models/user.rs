use diesel::pg::PgConnection;
use diesel::prelude::*;

use serde::Serialize;

use bcrypt::{DEFAULT_COST, hash};

use crate::schema::users;

#[derive(Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub email: String,
    pub passhash: String,
}

#[derive(Insertable)]
#[table_name = "users"]
struct NewUser<'a> {
    pub name: &'a str,
    pub email: &'a str,
    pub passhash: &'a str,
}

impl User {
    pub fn create_user<'a>(conn: &PgConnection, name: &'a str, email: &'a str, password: &'a str) -> User {
        let passhash = hash(password, DEFAULT_COST)
            .expect("Unable to hash password");

        let new_user = NewUser {
            name: name,
            email: email,
            passhash: &passhash,
        };

        diesel::insert_into(users::table)
            .values(&new_user)
            .get_result(conn)
            .expect("Error saving new user")
    }

    pub fn get_all(conn: &PgConnection) -> Vec<User> {
        use crate::schema::users::dsl::*; 

        let results = users.load::<User>(conn)
            .expect("Error loading users");

        results
    }
}